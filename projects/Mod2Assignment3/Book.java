/** Represents a book.
 *  Used emphasize Unit 2 concepts and help you prepare
 *  for Project 2.
 *  @author // TODO add your name here
 */
public class Book {

    // TODO Create private instance variables for title, author, publicationYear,
    // numberOfCopies, and size, meaning how thick is the book in cm?

    // TODO Create public getters and setters for each instance variable

    // TODO Create a method to calculate the width on the bookshelf that all copies
    // of this book take up (numberOfCopies * size)

    // TODO Create a method that returns a String containing the book information,
    // including the distance this title takes up.

    // TODO Remember to include descriptive comments before each method explaining
    // what the method does. Be sure to include details about what parameters the
    // methods take and what they return, if anything.

}
