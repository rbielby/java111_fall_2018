/**
 *  This class demonstrates the behavior of local
 *  vs. instance variables.
 */
public class LocalVsInstanceVariables {

    private String firstName = "Fred";

    public void demo(String name) {
        String firstName = name;
        System.out.println("local firstName: " + firstName);
        if (firstName.equals(this.firstName)) {
            System.out.println("They are equal");
        } else {
            System.out.println("They are NOT equal");
        }
    }

    public void run() {
        demo("Fred");
        firstName = "Barney";
        System.out.println("instance variable firstName: " + firstName);
    }
}
