/**
 *  This Dog class shows different behavior depending on size.
 *
 *@author    eknapp
 */
public class DogNotPrivate {

    int     size;
    String  name;

    /**
     *  This method is the code for the bark action
     *
     * @param barkNumber the number of barks to print out
     */
    void bark(int barkNumber) {

        while (barkNumber > 0) {
            if (size > 60) {
                System.out.println("Wooof! Woooof!");
            } else if (size > 14) {
                System.out.println("Ruff! Ruff!");
            } else {
                System.out.println("Yip! Yip!");
            }
            barkNumber --;
        }
    }
}
