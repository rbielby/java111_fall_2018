/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class MyClassTest {

    /**
     *  The main program for
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args) {
        MyClass myClass = new MyClass();

        int sum = myClass.calculateSum(1,2,3);
        if (sum == 6) {
            System.out.println("It\'s all good");
        } else {
            System.out.println("Fail!! Expected: 6 Actual: " + sum);
        }
    }

}
