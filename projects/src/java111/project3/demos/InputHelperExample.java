/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class InputHelperExample {

    /**
     *  The main program for
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args) {
        InputHelper helper = new InputHelper();
        String dogName = helper.getUserInput("What is you dogs name?");
        System.out.println("The Dogs name is " + dogName);
        System.out.println("The dogs name is Wilson " + dogName.equals("Wilson"));

        String stringDogAge = helper.getUserInput("What is the dogs age?");
        int dogAge = Integer.parseInt(stringDogAge);
        System.out.println("In human years: " + dogAge * 7);

        float aFloat = 123.44f;
    }

}
