/**
 * Example of overriding the equals method
 *
 * @author rjbielby
 */
public class Employee {

    private String name;
    private int employeeID;

    public boolean equals(Employee that) {
        if (that != null) {
            return that.getEmployeeID() == this.getEmployeeID();
        }
        return false;
    }

	/**
	* Returns value of name
	* @return
	*/
	public String getName() {
		return name;
	}

	/**
	* Sets new value of name
	* @param
	*/
	public void setName(String name) {
		this.name = name;
	}

	/**
	* Returns value of employeeID
	* @return
	*/
	public int getEmployeeID() {
		return employeeID;
	}

	/**
	* Sets new value of employeeID
	* @param
	*/
	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}
}
