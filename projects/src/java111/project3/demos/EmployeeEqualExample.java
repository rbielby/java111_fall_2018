import java.util.*;

/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class EmployeeEqualExample {

    /**
     *  The main program for
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args) {

        //ArrayList<Employee> emps = new ArrayList<Employee>();

        Employee emp1 = new Employee();
        emp1.setEmployeeID(123);
        emp1.setName("Randy");

        //emps.add(emp1);

        Employee emp2 = new Employee();
        emp2.setEmployeeID(444);
        emp2.setName("Randy");

        //emps.add(emp2);

        System.out.println("Are the employees the same???? " + emp1.equals(emp2));
    }

}
