/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class Cat {


    public String asleepOrAwake(int hour) {
        if (hour < 12) {
            return "Asleep";
        } else {
            return "Awake";
        }
    }

}
