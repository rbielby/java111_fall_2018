import java.util.*;

public class MyClass {

    public int calculateSumx(int number1, int number2, int number3) {
        int[] theNumbers = new int[3];
        theNumbers[0] = number1;
        theNumbers[1] = number2;
        theNumbers[2] = number3;
        int total = 0;
        for (int aNumber : theNumbers) {
            total += aNumber;
        }
        return total;
    }

    public int calculateSum(int number1, int number2, int number3) {
        ArrayList<Integer> theNumbers = new ArrayList<Integer>();
        theNumbers.add(number1);
        theNumbers.add(number2);
        theNumbers.add(number3);
        int total = 0;
        for (int aNumber : theNumbers) {
            total += aNumber;
        }
        return total;
    }

}
