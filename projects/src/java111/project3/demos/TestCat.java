/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class TestCat {
    private static String asleep = "Asleep";
    private static String awake = "Awake";
    /**
     *  The main program for
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args) {
        Cat cat = new Cat();
        String returnValue = cat.asleepOrAwake(11);

        if (returnValue.equals(asleep)) {
            System.out.println("All good");
        } else {
            System.out.println("Not good cat should be asleep");
        }

        returnValue = cat.asleepOrAwake(13);

        if (returnValue.equals(awake)) {
            System.out.println("All good");
        } else {
            System.out.println("Not good cat should awake");
        }

        returnValue = cat.asleepOrAwake(12);

        if (returnValue.equals(awake)) {
            System.out.println("All good");
        } else {
            System.out.println("Not good cat should awake");
        }


    }

}
