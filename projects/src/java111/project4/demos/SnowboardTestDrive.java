/**
 * Program to test out the Snowboard
 *
 * @author rjbielby
 */
public class SnowboardTestDrive {
    /**
     *  The main program for SnowboardTestDrive
     *
     *@param  args  The command line arguments
     */
    public static void main(String[] args) {
        Snowboard snowboard1 = new Snowboard();
        snowboard1.loseControl();
        snowboard1.shred();
        System.out.println(snowboard1.toString());

        Object snowboard2 = new Snowboard();
        //snowboard2.getAir();
        //
        // Snowboard snowboard3 = (Snowboard)snowboard2;
        // snowboard3.getAir();
    }
}
