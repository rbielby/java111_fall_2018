/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class Dog extends Canine implements Tail, Pet {

    public void eat() {
        System.out.println("Chomp");
    }

    public void wag() {
        System.out.println("Wiggle waggle");
    }

    public void snuggle() {

    }
}
