/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class Book {

    private String author;
    private String title;
    private String isbn;

    public void display() {
        System.out.println("Author : " + author );
        System.out.println("Title : " + title);
        System.out.println("ISBN : " + isbn);
    }
	/**
	* Returns value of author
	* @return
	*/
	public String getAuthor() {
		return author;
	}

	/**
	* Sets new value of author
	* @param
	*/
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	* Returns value of title
	* @return
	*/
	public String getTitle() {
		return title;
	}

	/**
	* Sets new value of title
	* @param
	*/
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	* Returns value of ISBN
	* @return
	*/
	public String getISBN() {
		return isbn;
	}

	/**
	* Sets new value of ISBN
	* @param
	*/
	public void setISBN(String isbn) {
		this.isbn = isbn;
	}
}
