/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public abstract class Canine {

    public void roam() {
        //do some roaming
    }

    public abstract void eat();
}
