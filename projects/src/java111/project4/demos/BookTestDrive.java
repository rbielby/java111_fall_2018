import java.util.*;

/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class BookTestDrive {

    /**
     *  The main program for
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args) {
        ArrayList<Book> books = new ArrayList<Book>();
        Book book = new Book();
        Book ebook = new EBook();
        Book audioBook = new AudioBook();


        books.add(book);
        books.add(ebook);
        books.add(audioBook);

        display(books);
    }

    private static void display(ArrayList<Book> books) {

        for (Book aBook : books) {
            aBook.display();
        }

        //book.display();
        //ebook.display();
        //audioBook.display();
    }


}
