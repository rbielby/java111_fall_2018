/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class Words {

    private String wordEntry;

    public boolean equals(Object that) {
        if (that != null) {
            if (that instanceof Words) {
                return ((Words)that).getWordEntry().equals(this.getWordEntry());
            }
        }
        return false;
    }

	/**
	* Returns value of wordEntry
	* @return
	*/
	public String getWordEntry() {
		return wordEntry;
	}

	/**
	* Sets new value of wordEntry
	* @param
	*/
	public void setWordEntry(String wordEntry) {
		this.wordEntry = wordEntry;
	}
}
