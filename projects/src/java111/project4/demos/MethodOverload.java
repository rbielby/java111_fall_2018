/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class MethodOverload {

    private long phoneNumber;

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        String numericPhoneNumber = phoneNumber.replaceAll("-", "");
        this.phoneNumber = Long.parseLong(numericPhoneNumber);
    }

	/**
	* Returns value of phoneNumber
	* @return
	*/
	public long getPhoneNumber() {
		return phoneNumber;
	}

}
