/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class Lawn {
    private int length;
    private int width;

    public Lawn() {
        //a;sldkjf;alsdkf;laksdjf;laskdjf
    }

    public Lawn(int length, int width) {
        this.length = length;
        this.width = width;
    }

	/**
	* Returns value of length
	* @return
	*/
	public int getLength() {
		return length;
	}

	/**
	* Returns value of width
	* @return
	*/
	public int getWidth() {
		return width;
	}

}
