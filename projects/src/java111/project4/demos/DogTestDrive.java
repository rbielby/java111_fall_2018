/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class DogTestDrive {

    /**
     *  The main program for
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args) {

        Tail dogTail = new Dog();
        wiggle(dogTail);

        Lawn lanw = new Lawn(100, 100);
    }

    public static void wiggle(Tail dog) {
        dog.wag();

    }


}
