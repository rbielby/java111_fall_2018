/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public interface Tail  {

    public void wag();

}
