/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class EBook extends Book {

    private String url;
    private int characterCount;

    public void display() {
        super.display();
        System.out.println("URL : " + url);
        System.out.println("Character Count : " + characterCount);
    }


	/**
	* Returns value of url
	* @return
	*/
	public String getUrl() {
		return url;
	}

	/**
	* Sets new value of url
	* @param
	*/
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	* Returns value of characterCount
	* @return
	*/
	public int getCharacterCount() {
		return characterCount;
	}

	/**
	* Sets new value of characterCount
	* @param
	*/
	public void setCharacterCount(int characterCount) {
		this.characterCount = characterCount;
	}
}
