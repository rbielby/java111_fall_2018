/**
 * The Snowboard class
 *
 * @author rjbielby
 */
public class Snowboard {
    /**
     *  Causes the board to get air
     *
     */
    public void getAir() {
        System.out.println("Woooo hoooo!!!!!");
    }

    /**
     * What happens before a crash
     *
     */
    public void loseControl() {
        System.out.println("No no no no, ahhhhhhhhhh");
    }

    /**
     * Makes a turn
     */
    public void turn() {
        System.out.println("Let\'s turn");
    }

    /**
     * Totally shreds it man
     */
    public void shred() {
        System.out.println("DUDE I'm shredding");
    }

}
