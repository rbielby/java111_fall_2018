public class Song {
    public String title;
    public String artist;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArtist(String inputArtist) {
        this.artist = inputArtist;
    }

    public void play() {
        System.out.println(artist + " plays " + title);
    }
}
