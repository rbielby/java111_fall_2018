/** Demonstrate a few primitives
 * @author pwaite
 */

public class PrimitiveDemo {
    /**
     * Create an int, a float, a char and display them
     * @param args
     */
    public static void main(String[] args) {
        int myInt = 60000;
        float myFloat = 799.99f;
        char myChar = 'a';

//        System.out.println("MyInt=> " + myInt);
//        System.out.println("MyFloat=> " + myFloat);
//        System.out.println("MyChar=> " + myChar);
//
//        System.out.println(myInt + myChar);
//        System.out.println(myInt + myFloat);
//
//        float myNewFloat = myInt + myFloat;
//        System.out.println("MyNewFloat=> " + myNewFloat);
//
//        int myNewInt = myInt + (int)myFloat;
//        System.out.println("myNewInt => " + myNewInt);

        // create an array of doubles, add some double values and display them

        double[] doubles = new double[4];
        doubles[0] = 8.99;
        doubles[1] = 7.99;
        doubles[2] = 10.99;
        doubles[3] = 14.99;
       // doubles[4] = 8.00;

        System.out.println("The first item is: " + doubles[0]);

        for (int counter = 0; counter <= doubles.length; counter++ ) {
            System.out.println("The item is " + doubles[counter]);
        }

    }
}
