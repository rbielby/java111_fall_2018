public class AssignmentDemo {
    public static void main(String[] args) {
        int myInt = 500;
        double myDouble = 5.55;
        String myString = "Hello";

        System.out.println(myInt);
        System.out.println(myDouble);
        System.out.println(myString);
    }
}
