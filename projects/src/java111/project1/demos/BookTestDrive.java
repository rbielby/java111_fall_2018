/** Create a couple books, swap
 *  references to demonstrate object
 *  reference use and some NPE (NullPointerExceptions)
 */
public class BookTestDrive {
    /**
     * Instantiate 2 books objects
     * Display the books
     * Assign one book to the other book
     * Display again
     * Change the title of one book
     * Display again
     * @param args
     */

    public static void main(String[] args) {

        Book book1 = new Book();
        book1.title = "2001";
        book1.author = "Arthur C Clark";
        book1.numberOfPages = 250;

        Book book2 = new Book();
        book2.title = "Lord Of the Rings";
        book2.author = "J.R.R. Tolkien";
        book2.numberOfPages = 1000;

        book1.display();
        book2.display();

        book2 = book1;

        System.out.println();
        book1.display();
        book2.display();

        book1.title = "Head First Java";

        System.out.println();
        book1.display();
        book2.display();

        book2 = null;

        System.out.println();
        book2.display();
        book1.display();




    }

}
