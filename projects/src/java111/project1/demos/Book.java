/**
 * Does this works
 * @author rjbielbyd
 */

public class Book {
    public String title;
    public String author;
    public int numberOfPages;

    public void display() {
        System.out.println("Author: " + author + " Title: " + title + " Number of Pages: " + numberOfPages);
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }
}
