/** Represents a book, used to demonstrate obj ref variables
 * @pwaite
 */

public class Book {
    String title;
    String author;
    int numberOfPages;


    /** displays the book's details
     *
     */
    public void display() {
        System.out.println("The book, " + title + " by " + author +
                " has " + numberOfPages + " pages.");

    }


}
