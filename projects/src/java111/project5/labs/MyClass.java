/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class MyClass {

    public MyClass() {
        this("help");
        System.out.println("This is the default");

    }

    public MyClass(String name) {
        this();
        System.out.println("MyClass");
    }

}
