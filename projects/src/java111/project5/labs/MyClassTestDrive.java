/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class MyClassTestDrive {

    /**
     *  The main program for
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args) {
        MyClass myClass = new MyClass("Randy");
        MySubClass mySubClass = new MySubClass("Lisa");
    }


}
