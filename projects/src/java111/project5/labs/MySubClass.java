/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class MySubClass extends MyClass {

    public MySubClass(String name) {
        super(name);
        System.out.println("MySubClass");
    }

}
