import java.io.*;

/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class TryCatchTestDrive {

    public static void main(String[] args) {
        TryCatch tryCatch = new TryCatch();
        try {
            tryCatch.doIt();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {

        }

    }
}
