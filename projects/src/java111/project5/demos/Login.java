import java.util.*;

/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class Login {
    public static final int MAX_USERS = 5;
    private static int totalUsersLoggedOn;
    private static List<User> loggedOnUsers = new ArrayList<User>();

    public static boolean logUserIn(User user) {
        if (totalUsersLoggedOn < MAX_USERS) {
            loggedOnUsers.add(user);
            totalUsersLoggedOn++;
            return true;
        } else {
            return false;
        }
    }

    public static int getTotalUsersLoggedOn() {
        return totalUsersLoggedOn;
    }

}
