/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class NoDefaultConstructor {
    private String name;
    private int age;

    public NoDefaultConstructor() {
        this.name = "Randy";
        this.age = 29;
    }
    public NoDefaultConstructor(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void NoDefaultConstructor(long height) {

    }
    
	/**
	* Returns value of name
	* @return
	*/
	public String getName() {
		return name;
	}

	/**
	* Returns value of age
	* @return
	*/
	public int getAge() {
		return age;
	}
}
