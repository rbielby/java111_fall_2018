/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class User {

    private String userName;
    private int hashCode;

	/**
	* Returns value of userName
	* @return
	*/
	public String getUserName() {
		return userName;
	}

	/**
	* Sets new value of userName
	* @param
	*/
	public void setUserName(String userName) {
        int hashCode = userName.hashCode();
        this.hashCode = hashCode;
		this.userName = userName;
	}
}
