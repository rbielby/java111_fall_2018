/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class StaticTest {

    public static final String MY_NAME = "Randy";

    /**
     *  The main program for
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args) {
        System.out.println("The value of PI is " + Math.PI);
        System.out.println("Here\'s a random number " + Math.random());
        System.out.println("Which is bigger 1000 or 100 " + Math.max(1000, 100));
    }


}
