import java.io.*;

/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class TryCatch {

    /**
     * I'm doing it
     * @throws IOException couldn't read file
     */
    public void doIt() throws IOException {
        int a = 1 + 1;
        if (a == 2) {
            throw new IOException("This is really bad");
        }
    }
}
