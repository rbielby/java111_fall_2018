/**
 * Program javadoc goes here.  Tell what the class is/does
 *
 * @author rjbielby
 */
public class NoDefaultConstructorTester {

    /**
     *  The main program for
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args) {

        String string1 = new String();
        String string2 = new String("string value");

        NoDefaultConstructor demo = new NoDefaultConstructor("Wilson", 2);
        System.out.println(demo.getName());
        System.out.println(demo.getAge());

        //NoDefaultConstructor whatsUp = new NoDefaultConstructor(123L);
    }


}
