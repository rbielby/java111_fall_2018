# Module 1 Videos

## Course Info and Set Up (45:49)

* [Welcome, Blackboard and Slack](https://youtu.be/tB0v6ZkDBw8) (13:14)
* [Course Website, Calendar, and Syllabus](https://youtu.be/zUiNwKv3D1E) (15:06)
* [JDK Install](https://youtu.be/xQDfxITCaAE) (14:41)
* [Atom Install](https://youtu.be/733kbzCR6eY) (2:48)

## Week 1 (1:41:12)
* [Introducing the Command Line](https://youtu.be/_U90BxckpKQ) (12:12)
* [Our First Java Program](https://youtu.be/oAaonQDu6Wo) (16:03)
* [The Scripts](https://youtu.be/oAntkFSMvpU) (15:50)
* [Writing our First Java Class](https://youtu.be/sZAAGBPwzoY) (25:49)
* [Assignment](https://youtu.be/xlb8Z-IBhYI) (10:30)
* [Booleans and Conditionals](https://youtu.be/g8M8EX3ARyA) (12:24)
* [Submitting Work](https://youtu.be/IDom4MmiSsU) (8:24) 

## Week 2 (29:15)

* [Looping](https://youtu.be/YvODzzgRb00) (11:19)
* [Project 1 Overview](https://youtu.be/r1LQ9UfXupk) (5:38)
* [How to Ask for Programming Help](https://youtu.be/4xy5rXNGvUM) (5:12)
* [The Beauty of Dropbox](https://youtu.be/Y85G_dZTHoY) (2:41)
* [Accessing Demo Code](https://youtu.be/DKXUT6TEEKA) (4:25)

## Week 3 (1:05:27)
* [Intro to OO](https://youtu.be/_q1kAEnqluM) (10:01)
* [Classes](https://youtu.be/gVp3IxzTkR0) (6:26)
* [Objects](https://youtu.be/9CHlRvr0E2w) (3:56)
* [Coding Classes and Objects](https://youtu.be/clhiepeQ_Js) (13:16)
* [Vehicle and VehicleTestDrive Demo](https://youtu.be/kdIvkIwLpAw) (22:34)
* [Vehicle Demo in the Debugger](https://youtu.be/mvncyY7KoF0) (9:14)
